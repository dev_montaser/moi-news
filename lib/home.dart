import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:moi/pages/maps.dart';
import 'package:moi/pages/page.dart';
import 'package:moi/pages/more.dart';
import 'package:moi/pages/search.dart';
import 'package:moi/new_chat/widgets/sound.dart';
import 'package:provider/provider.dart';
import 'package:moi/assets/moi_icons_icons.dart';
import 'package:moi/new_chat/chathome.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:moi/settings/settings.dart';
import 'package:moi/settings/app_localizations.dart';
import 'package:moi/model/theme_model.dart';
import 'package:moi/pages/NewsList.dart';
import 'package:http/http.dart' as http;

const themeColor = Color(0xff1963b6);
const iconColor = Colors.white;
const themeGradient = Color(0x591963b6);
const themeFooter = Colors.black;

class Home extends StatefulWidget {
  ThemeModel model;

  Home(@required this.model);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  var dateFormatted;
  var deviceHeight;
  var deviceWidth;
  bool isLoading = false;
  SharedPreferences prefs;
  TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 4, vsync: this);

    attemptLogIn();
  }

  //

////////loginfire//////////
  Future<String> attemptLogIn() async {
    prefs = await SharedPreferences.getInstance();
/*String token=prefs.getString("token");
    this.setState(() {
      isLoading = true;
    });
    var res = await http.get(
        "https://stgnms.moi.gov.ae/api/Mobile/GetUrgentReports",
      headers: {
        'Authorization':'Bearer ${token}'
      },
    );
    var jsonslide=jsonDecode(res.body);
    print('body ${jsonslide}');*/
    /* if(res.statusCode == 200) {
      prefs.setString("username", email);
      prefs.setString("password", password);
      try {
        final loggedUser =
        await _auth.signInWithEmailAndPassword(
            email: 'gergess@yahoo.com', password: password);
        if (loggedUser != null) {
          setState(() {
            isLoading = false;
          });
          prefs.setString("id", loggedUser.user.uid);
          /* Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home(widget.model)));*/
          Navigator.pushReplacementNamed(context, 'home');

        }else{
          final newUser =
          await _auth.createUserWithEmailAndPassword(
            email: email, password: password,);
          if (newUser != null) {
            /*UpdateUserInfo info = UpdateUserInfo();
                              info.displayName = name;
                              await newUser.user.updateProfile(info);*/
            setState(() {
              isLoading = false;
            });

            prefs.setString("id", newUser.user.uid);
            /* Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home(widget.model)));*/
            Navigator.pushReplacementNamed(context, 'home');
          }else{
            EdgeAlert.show(context,
                title: 'Login Failed',
                description: 'error in firebase',
                gravity: EdgeAlert.BOTTOM,
                icon: Icons.error,
                backgroundColor: widget.model.primaryMainColor);
          }
        }
      } catch (e) {
        try{
          final newUser =
          await _auth.createUserWithEmailAndPassword(
            email: 'gergess@yahoo.com', password: password,);
          if (newUser != null) {
            /*UpdateUserInfo info = UpdateUserInfo();
                              info.displayName = name;
                              await newUser.user.updateProfile(info);*/
            setState(() {
              isLoading = false;
            });

            prefs.setString("id", newUser.user.uid);
            /* Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home(widget.model)));*/
            Navigator.pushReplacementNamed(context, 'home');
          }else{
            setState(() {
              isLoading = false;
            });
            print(e.toString());
            EdgeAlert.show(context,
                title: 'Login Failed',
                description: e.toString(),
                gravity: EdgeAlert.BOTTOM,
                icon: Icons.error,
                backgroundColor: widget.model.primaryMainColor);

          }
        }catch(e){
          EdgeAlert.show(context,
              title: 'Login Failed',
              description: e.toString(),
              gravity: EdgeAlert.BOTTOM,
              icon: Icons.error,
              backgroundColor: widget.model.primaryMainColor);
        }


      }
    }else{
      this.setState(() {
        isLoading = false;
      });
      EdgeAlert.show(context,
          title: 'Login Failed',
          description: jsonslide['error_description'],
          gravity: EdgeAlert.BOTTOM,
          icon: Icons.error,
          backgroundColor: widget.model.primaryMainColor);
    }*/
    return null;
  }

  ///////////////////
  @override
  Widget build(BuildContext context) {
    dateFormatted = formatDate(DateTime.now(), [d, ' ', MM, ' ', yyyy]);
    deviceHeight = MediaQuery.of(context).size.height;
    deviceWidth = MediaQuery.of(context).size.width;
    PageController _pageController = PageController();
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
          builder: (context, model, __) {
            //  print("Value of primary main color is: ${model.primaryMainColor}");
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              home: Scaffold(
                backgroundColor: model.primaryMainColor,
                body: Directionality(
                  textDirection: model.appLocal.languageCode == "ar"
                      ? TextDirection.rtl
                      : TextDirection.ltr,
                  child: TabBarView(
                    controller: _tabController,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                //building background
                                Container(
                                  width: deviceWidth,
                                  height: deviceHeight * 0.22,
                                  decoration: BoxDecoration(
                                    image: new DecorationImage(
                                      colorFilter: ColorFilter.mode(
                                          Colors.black.withOpacity(0.2),
                                          BlendMode.dstATop),
                                      image: new AssetImage(
                                          'images/newsbackground.jpg'),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),

                                //MOI logo
                                Column(
                                  children: [
                                    SizedBox(
                                      height: deviceHeight * 0.05,
                                    ),
                                    Container(
                                      alignment: Alignment.center,
                                      height: deviceHeight * 0.13,
                                      decoration: BoxDecoration(
                                        image: new DecorationImage(
                                          image: new AssetImage(
                                              'images/logo_only.png'),
                                          fit: BoxFit.fitHeight,
                                        ),
                                      ),
                                    ),
                                    Text('$dateFormatted',
                                        style: TextStyle(
                                            fontSize: 15, color: iconColor)),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          // SizedBox(
                          //   height: 10,
                          // ),
                          GestureDetector(
                            onTap: () {
                              /*  if(model!=null)
                              Navigator.push(
                                  context, MaterialPageRoute(builder: (context) => SearchPage( model)));*/

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Sound()));
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFFe8e8e8),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              padding: EdgeInsets.only(left: 10, right: 10),
                              width: MediaQuery.of(context).size.width * 0.80,
                              child: TextField(
                                style: TextStyle(fontSize: 13),
                                onSubmitted: (value) {},
                                enabled: false,
                                decoration: InputDecoration(
                                  icon: Icon(
                                    Icons.search,
                                  ),
                                  border: InputBorder.none,
                                  hintText: AppLocalizations.of(context)
                                      .translate('search'),
                                  hintStyle: TextStyle(
                                    fontFamily: 'Cairo-Regular',
                                  ),
                                  // filled: true,
                                ),
                              ), /*TextField(
                                style: TextStyle(
                                  fontSize: 15,

                                ),

                                textDirection: model.appLocal.languageCode=='ar'?TextDirection.rtl:
                                TextDirection.ltr,

                                decoration: InputDecoration(

                                  hintText: AppLocalizations.of(context).translate('search'),
                                  filled: true,
                                  fillColor: Color(0xFFe8e8e8),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    borderSide: BorderSide(color: Colors.grey.shade300),
                                  ),
                                ),
                              ),*/
                            ),
                          ),

                          Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () {
                                      if (model != null)
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => NewsList(
                                                      title: AppLocalizations
                                                              .of(context)
                                                          .translate('daily'),
                                                      model: model,
                                                      type: 1,
                                                    )));
                                    },
                                    child: CustomBox(
                                        title: AppLocalizations.of(context)
                                            .translate('daily'),
                                        iconHome: MoiIcons.news),
                                  ),
                                ),
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () {
                                      if (model != null)
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => NewsList(
                                                      title:
                                                          AppLocalizations.of(
                                                                  context)
                                                              .translate(
                                                                  'important'),
                                                      model: model,
                                                      type: 6,
                                                    )));
                                    },
                                    child: CustomBox(
                                        title: AppLocalizations.of(context)
                                            .translate('important'),
                                        iconHome: MoiIcons.important),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                    child: GestureDetector(
                                  onTap: () {
                                    if (model != null)
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => NewsList(
                                                    title: AppLocalizations.of(
                                                            context)
                                                        .translate('economy'),
                                                    model: model,
                                                    type: 2,
                                                  )));
                                  },
                                  child: CustomBox(
                                      title: AppLocalizations.of(context)
                                          .translate('economy'),
                                      iconHome: MoiIcons.economic),
                                )),
                                Expanded(
                                    child: GestureDetector(
                                  onTap: () {
                                    if (model != null)
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => NewsList(
                                                    title: AppLocalizations.of(
                                                            context)
                                                        .translate('breaking'),
                                                    model: model,
                                                    type: 5,
                                                  )));
                                  },
                                  child: CustomBox(
                                      title: AppLocalizations.of(context)
                                          .translate('breaking'),
                                      iconHome: MoiIcons.breaking),
                                )),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              textDirection: model.appLocal.languageCode == "ar"
                                  ? TextDirection.rtl
                                  : TextDirection.ltr,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () {
                                      if (model != null)
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => NewsList(
                                                      title: AppLocalizations
                                                              .of(context)
                                                          .translate('read'),
                                                      model: model,
                                                      type: 4,
                                                    )));
                                    },
                                    child: CustomBox(
                                        title: AppLocalizations.of(context)
                                            .translate('read'),
                                        iconHome: MoiIcons.publication),
                                  ),
                                ),
                                Expanded(
                                  child: GestureDetector(
                                    onTap: () {
                                      if (model != null)
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => NewsList(
                                                      title: AppLocalizations
                                                              .of(context)
                                                          .translate('affair'),
                                                      model: model,
                                                      type: 3,
                                                    )));
                                    },
                                    child: CustomBox(
                                        title: AppLocalizations.of(context)
                                            .translate('affair'),
                                        iconHome: MoiIcons.falcon),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      prefs != null
                          ? HomeScreen(
                              currentUserId: prefs.getString("id"),
                              model: model)
                          : Center(
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.greenAccent,
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(themeColor),
                              ),
                            ),
                      model != null
                          ? Setting(model, context)
                          : Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(themeColor),
                              ),
                            ),
                      model != null
                          ? More(model, context)
                          : Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(themeColor),
                              ),
                            ),
                    ],
                  ),
                ),
                bottomNavigationBar: Directionality(
                    textDirection: model.appLocal.languageCode == "ar"
                        ? TextDirection.rtl
                        : TextDirection.ltr,
                    child: ConvexAppBar(
                      items: [
                        TabItem(
                            icon: Icons.home,
                            title:
                                AppLocalizations.of(context).translate('home')),
                        TabItem(
                            icon: Icons.chat,
                            title:
                                AppLocalizations.of(context).translate('chat')),
                        TabItem(
                            icon: Icons.settings,
                            title: AppLocalizations.of(context)
                                .translate('bsetting')),
                        TabItem(
                            icon: Icons.more,
                            title:
                                AppLocalizations.of(context).translate('more')),
                      ],
                      backgroundColor: model.primaryMainColor,
                      controller: _tabController,
                      onTap: (int index) => {
                        _tabController.animateTo(index,
                            curve: Curves.fastLinearToSlowEaseIn,
                            duration: Duration(milliseconds: 600))
                      },
                    )
                    /* CustomBottomNavigationBar(
                  backgroundColor: model.primaryMainColor,
                  itemBackgroudnColor: model.primaryMainColor,
                  items: [
                    CustomBottomNavigationBarItem(
                      icon: Icons.home,
                      title: AppLocalizations.of(context).translate('home'),
                    ),
                    CustomBottomNavigationBarItem(
                      icon: Icons.chat,
                      title: AppLocalizations.of(context).translate('chat'),
                    ),
                    CustomBottomNavigationBarItem(
                      icon: Icons.settings,
                      title: AppLocalizations.of(context).translate('bsetting'),
                    ),
                    CustomBottomNavigationBarItem(
                      icon: Icons.more,
                      title: AppLocalizations.of(context).translate('more'),
                    ),
                  ],

                  onTap: (index) {
                    _pageController.animateToPage(index,
                        curve: Curves.fastLinearToSlowEaseIn,
                        duration: Duration(milliseconds: 600));
                  },

                ),*/
                    ),
              ),
            );
          },
        ));
  }
}

class CustomBox extends StatelessWidget {
  const CustomBox({@required this.title, @required this.iconHome});

  final title;

  final iconHome;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      padding: EdgeInsets.all(1),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        /*  gradient: RadialGradient(
          radius: 0.9,
          colors: [Color(0x59ffffff), Colors.white10 ],
        ),*/
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 7.0),
            child: Icon(
              iconHome,
              size: 68,
              color: iconColor,
            ),
          ),
          Expanded(
            child: Text(
              title,
              textAlign: TextAlign.center,
              maxLines: 2,
              textScaleFactor: .85,
              overflow: TextOverflow.visible,
              style: TextStyle(
                color: Colors.white,
                fontSize: 22.0,
                wordSpacing: .3,
                fontWeight: FontWeight.bold,
                fontFamily: 'Hacen',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
