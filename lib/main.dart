import 'package:flutter/material.dart';
import 'package:moi/new_chat/chathome.dart';
import 'package:moi/new_chat/shareMenu.dart';
import 'package:moi/pages/signup.dart';
import 'package:moi/home.dart';
import 'package:moi/test.dart';
import './pages/login.dart';
import 'package:firebase_core/firebase_core.dart';
import './pages/splash.dart';
import 'package:provider/provider.dart';
import 'package:moi/model/theme_model.dart';
import 'package:moi/pages/splash.dart';
import 'package:moi/settings/app_localizations.dart';

// import 'package:moi/pages/sound.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() async {
  //GoogleMap.init('AIzaSyDvaS7W8iRIZTGJ6v5yePMWF4B2sCEVWqg');
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  // await FirebaseApp.configure(name: "moi-flutter", options: null);
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider<ThemeModel>(
        create: (_) => ThemeModel(),
      ),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ThemeModel>(
        create: (BuildContext context) => ThemeModel(),
        child: Consumer<ThemeModel>(builder: (context, model, __) {
          model.checkLogin();
          // print('login ${model.login}');
          return Directionality(
            textDirection: model.appLocal == Locale('ar')
                ? TextDirection.rtl
                : TextDirection.ltr,
            child: MaterialApp(
              locale: model.appLocal,
              localizationsDelegates: [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: [
                const Locale('en', 'US'),
                const Locale('ar', ''),
              ],

              title: 'News Aggregator',
              debugShowCheckedModeBanner: false,
              darkTheme: ThemeData.dark(),
              theme: ThemeData(
                  dividerColor: model.dividerColor,
                  textTheme: Theme.of(context).textTheme.apply(
                      bodyColor: model.textColor,
                      displayColor: model.textColor),
                  appBarTheme: AppBarTheme(color: model.appbarcolor),
                  primaryColor: model.primaryMainColor,
                  accentColor: model.accentColor,
                  brightness: Brightness.light),
              // home: Home(),
              initialRoute: model?.login ? 'home' : 'login',
              //initialRoute:'home',
              routes: {
                'splash': (context) => ChatterHome(model: model),
                'login': (context) => LoginScreen(
                      model: model,
                    ),
                'chat': (context) => HomeScreen(),
                'signup': (context) => ChatterSignUp(model: model),
                'home': (context) => Home(model),
                'test': (context) => Test(),
                //  'map': (context) => SoundPage(),
              },
            ),
          );
        }));
  }
}
