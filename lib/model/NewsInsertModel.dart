class NewsInsert {
  String title;
  String content;
  String image;
  String date;
  String sourceLink;

  // NewsInsert(this.title, this.date, this.description, this.imglink);
  NewsInsert(this.title, this.content, this.image, this.date, this.sourceLink);
}
