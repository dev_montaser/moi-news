// import 'package:chat_app/pages/chat.dart';
import 'package:flutter/material.dart';
import 'package:moi/pages/signup.dart';

import '../pages/splash.dart';

class ChatterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Chatter',

      theme: ThemeData(
        textTheme: TextTheme(
          body1: TextStyle(fontFamily: 'Poppins'),
        ),
      ),
      // home: ChatterHome(),
      initialRoute: '/',
      routes: {
        '/': (context) => ChatterHome(),
        '/signup': (context) => ChatterSignUp(),
        // '/chats':(context)=>ChatterScreen()
      },
    );
  }
}
