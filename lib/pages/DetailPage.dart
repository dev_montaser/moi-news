import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:moi/model/NewsInsertModel.dart';
import 'package:moi/pages/NewsSummary.dart';
import 'package:moi/utils/separator.dart';
import 'package:moi/utils/text_style.dart';

import 'maps.dart';

class DetailPage extends StatefulWidget {
  final NewsInsert news;
  final int color;
  final int lang;
  final double size;

  DetailPage(this.news, this.color, this.lang, this.size);

  @override
  _GooMapState createState() => _GooMapState();
}

class _GooMapState extends State<DetailPage> {
  bool showMap = false;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        //  constraints: new BoxConstraints.expand(),
        color: Colors.white,
        child: new Stack(
          children: <Widget>[
            showMap ? _showMap(context) : Container(),
            //_getGradient(),
            _getContent(context),
            _getToolbar(context),
          ],
        ),
      ),
    );
  }

  Container _getBackground() {
    return new Container(
      child: new Image.network(
        widget.news.image,
        fit: BoxFit.cover,
        height: 300.0,
      ),
      constraints: new BoxConstraints.expand(height: 295.0),
    );
  }

  Widget _showMap(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Container(
        //constraints: new BoxConstraints.expand(height: 295.0),
        height: MediaQuery.of(context).size.height * .5,
        child: GooMap(
          location: LatLng(-20.131886, -47.484488),
          color: widget.color,
        ),
      ),
    );
  }

  Container _getGradient() {
    return new Container(
      margin: new EdgeInsets.only(top: 190.0),
      height: 110.0,
      decoration: new BoxDecoration(
        gradient: new LinearGradient(
          colors: <Color>[Colors.white, Colors.white],
          stops: [0.0, 0.9],
          begin: const FractionalOffset(0.0, 0.0),
          end: const FractionalOffset(0.0, 1.0),
        ),
      ),
    );
  }

  Container _getContent(BuildContext context) {
    final _overviewTitle =
        widget.lang == 1 ? "التفاصيل" : "Detail".toUpperCase();
    double marginTop = showMap ? MediaQuery.of(context).size.height * .51 : 60;
    return new Container(
      padding: EdgeInsets.fromLTRB(0.0, marginTop, 0.0, 32.0),
      child: new ListView(
        //  padding: new
        children: <Widget>[
          /* showMap?
         Container(
          height: MediaQuery.of(context).size.height*.5,
          child: GooMap(location: LatLng(-20.131886, -47.484488)),
              constraints: new BoxConstraints.expand(height: 295.0)

     ):Container(),*/
          new NewsSummary(
            widget.news,
            widget.color,
            widget.lang,
            widget.size,
            horizontal: false,
          ),
          new Container(
            padding: new EdgeInsets.symmetric(horizontal: 32.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  _overviewTitle,
                  style: Style.headerTextStyle,
                ),
                new Separator(),
                new Text(widget.news.content,
                    style: TextStyle(
                        fontSize: widget.size,
                        fontFamily: 'Cairo-Regular',
                        fontWeight: FontWeight.w400)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _getToolbar(BuildContext context) {
    return new Container(
      decoration: BoxDecoration(color: Color(widget.color)),
      margin: new EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          new BackButton(color: Colors.white),
          Row(
            children: [
              Text(
                widget.lang == 1 ? "اظهار الخريطة" : 'Show map',
                style: TextStyle(
                    fontFamily: 'Cairo-Regular',
                    fontSize: 16,
                    color: Colors.white),
              ),
              Switch(
                  activeColor: Colors.white,
                  value: showMap,
                  onChanged: (val) {
                    setState(() {
                      showMap = val;
                    });
                  }),
            ],
          )
        ],
      ),
    );
  }
}
