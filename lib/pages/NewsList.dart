import 'dart:convert';

import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:moi/model/NewsInsertModel.dart';
import 'package:moi/model/theme_model.dart';
import 'package:moi/pages/NewsSummary.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:moi/settings/app_localizations.dart';
import 'package:provider/provider.dart';

class NewsList extends StatefulWidget {
  ThemeModel model;
  int type;

  NewsList({Key key, this.title, this.model, this.type}) : super(key: key);
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<NewsList> {
  List<NewsInsert> newsInsert = [];
  bool isLoading = false;
  var dateFormatted;
  DateTime selectedDate;

/////////
  String removeAllHtmlTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return htmlText.replaceAll(exp, '');
  }

  Future getProvince() async {
    //loadingView();

    setState(() {
      isLoading = true;
    });
    try {
      var res = await http.post("https://stgnms.moi.gov.ae/connect/token",
          body: {
            "username": 'cmanager',
            "password": '12345678',
            "grant_type": 'password'
          });
      var jsonslide = jsonDecode(res.body);
      if (res.statusCode == 200) {
        var result = await http.get(
          "https://stgnms.moi.gov.ae/api/Mobile/GetCurrentNewsLetter?date=${selectedDate.toIso8601String().split(' ')[0]}&type=${widget.type}",
          headers: {'Authorization': 'Bearer ${jsonslide['access_token']}'},
        );
        var newsModel = jsonDecode(removeAllHtmlTags(result.body));
        //var newsModel = jsonEncode(result.body);

        List<NewsInsert> news = [];

        for (var n in newsModel) {
          print("i data:${n}");
          NewsInsert insert = new NewsInsert(
              n["title"], n["content"], n["image"], n["date"], n["sourceLink"]);
          /*newsList[i].description,
            newsList[i].urlToImage);*/
          news.add(insert);
        }
        setState(() {
          newsInsert = news;
          isLoading = false;
        });

        //   print('${newsInsert.length}');
      } else {
        setState(() {
          isLoading = false;
        });
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      print(e);
    } finally {
      setState(() {
        isLoading = false;
      });
    }
    setState(() {
      isLoading = false;
    });
    return null;
  }

  ////////////
  /*Future<int> dioCall() async {
    loadingView();
    try {
      var response = await http.get(
          "http://newsapi.org/v2/top-headlines?sources=google-news&apiKey=3be1d3eb6d4e43128635b41f5aaff854");
      print('lara ${response.body}');
      final newsModel = newsModelFromJson(response.body.toString());

      var list = newsModel.toJson()['articles'] as List;
      List<News> newsList = list.map((i) => News.fromJson(i)).toList();
      List<NewsInsert> newsInsertTemp = [];
      newsInsertTemp.clear();
      for (var i = 0; i < newsList.length; i++) {
        print("i data:${i}");
        NewsInsert insert = new NewsInsert(
            newsList[i].title,
            newsList[i].description,
            newsList[i].description,
            newsList[i].urlToImage);
        newsInsertTemp.add(insert);
      }
      newsInsert.clear();
      setState(() {
        newsInsert.addAll(newsInsertTemp);
      });
    } catch (e) {
      print(e);
    }
    return 10;
  }*/

  @override
  void initState() {
    loadingView();
    //  apiCall();
    selectedDate = DateTime.now();
    getProvince();
    super.initState();
  }

  _selectDate() {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(2018, 3, 5),
        maxTime: DateTime(2030, 1, 1),
        theme: DatePickerTheme(
            headerColor: Colors.orange,
            backgroundColor: Colors.blue,
            itemStyle: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
            doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
        onChanged: (date) {
      print('change $date in time zone ' +
          date.timeZoneOffset.inHours.toString());
    }, onConfirm: (date) {
      print('confirm $date');
      setState(() {
        selectedDate = date;
      });
      getProvince();
    },
        currentTime: DateTime.now(),
        locale: widget.model.appLocal.languageCode == 'ar'
            ? LocaleType.ar
            : LocaleType.en);
  }

  @override
  Widget build(BuildContext context) {
    dateFormatted = formatDate(selectedDate, [yyyy, '-', MM, '-', dd]);

    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
            builder: (_, model, __) => Scaffold(
                  appBar: AppBar(
                    centerTitle: true,
                    backgroundColor: model.primaryMainColor,
                    title: Text(
                      widget.title,
                      style: TextStyle(fontSize: 20, fontFamily: 'Cairo-Bold'),
                    ),
                  ),
                  body: new Column(children: <Widget>[
                    Align(
                      alignment: Alignment.topCenter,
                      child: GestureDetector(
                        onTap: () {
                          _selectDate();
                        },
                        child: Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 100, vertical: 7),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.black12),
                            padding: EdgeInsets.all(8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Flexible(
                                  child: Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.blue,
                                    size: 25,
                                  ),
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                Icon(
                                  Icons.date_range,
                                  color: Colors.blue,
                                  size: 25,
                                ),
                                Text(
                                  '${selectedDate.toString().split(' ')[0]}',
                                  style: TextStyle(
                                      color: Colors.blue, fontSize: 18),
                                ),
                              ],
                            )),
                      ),
                    ),
                    new Center(
                      // color: model.primaryMainColor,
                      child: !isLoading
                          ? (newsInsert.length > 0
                              ? new CustomScrollView(
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  slivers: <Widget>[
                                    new SliverPadding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10.0),
                                      sliver: new SliverList(
                                        delegate:
                                            new SliverChildBuilderDelegate(
                                          (context, index) => new NewsSummary(
                                              newsInsert[index],
                                              model.primaryMainColor.value,
                                              model.appLocal.languageCode ==
                                                      'ar'
                                                  ? 1
                                                  : 2,
                                              model.article_size),
                                          childCount: newsInsert.length,
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : Column(
                                  children: [
                                    SizedBox(
                                      height: 100,
                                    ),
                                    loadingViewEm()
                                  ],
                                ))
                          : Column(
                              children: [
                                SizedBox(
                                  height: 100,
                                ),
                                loadingView()
                              ],
                            ),
                    ),
                  ]),
                  floatingActionButton: FloatingActionButton(
                    onPressed: () {
                      loadingView();
                      // apiCall();
                      getProvince();
                    },
                    child: Icon(Icons.refresh),
                    backgroundColor: Colors.blue,
                  ),
                )));
  }

  // Progress indicator widget to show loading.
  Widget loadingView() {
    return Center(
      child: CircularProgressIndicator(
        backgroundColor: Colors.blue,
      ),
    );
  }

  Widget loadingViewEm() {
    return Column(
      children: [
        Icon(
          Icons.filter_drama,
          size: 50,
          color: Colors.grey,
        ),
        Text(
          widget.model.appLocal.languageCode == 'ar'
              ? 'لا توجد أخبار'
              : 'There is no news',
          style: TextStyle(
              fontStyle: FontStyle.italic, color: Colors.grey, fontSize: 18),
        ),
      ],
    );
  }
}
