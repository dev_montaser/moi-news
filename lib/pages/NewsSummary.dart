import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:moi/new_chat/shareMenu.dart';
import 'package:moi/model/NewsInsertModel.dart';
import 'package:moi/pages/DetailPage.dart';

class NewsSummary extends StatelessWidget {
  final NewsInsert news;
  final bool horizontal;
  final int color;
  final int lang;
  final double size;

  NewsSummary(this.news, this.color, this.lang, this.size,
      {this.horizontal = true});

  NewsSummary.vertical(this.news, this.color, this.lang, this.size)
      : horizontal = false;

  @override
  Widget build(BuildContext context) {
    final newsThumbnail = new Container(
      margin: new EdgeInsets.symmetric(vertical: 10.0),
      alignment: horizontal
          ? lang == 1
              ? FractionalOffset.centerRight
              : FractionalOffset.centerLeft
          : FractionalOffset.center,
      child: new Hero(
        tag: "news-hero-${news.title}",
        child: new Container(
            width: 100.0,
            height: 100.0,
            decoration: new BoxDecoration(
                shape: BoxShape.circle,
                image: new DecorationImage(
                    fit: BoxFit.fill, image: new NetworkImage(news.image)))),
      ),
    );

    final newsCardContent = new Container(
      margin: new EdgeInsets.fromLTRB(
          horizontal ? (lang == 2 ? 65.0 : 10) : 16.0,
          horizontal ? 16.0 : 42.0,
          (lang == 1 ? 65.0 : 10),
          10.0),
      constraints: new BoxConstraints.expand(),
      child: new Column(
        crossAxisAlignment:
            horizontal ? CrossAxisAlignment.start : CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Container(height: 4.0),
          new Text(
            news.title,
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Hacen_Liner",
                fontSize: 15.0,
                fontWeight: FontWeight.w600),
            overflow: TextOverflow.ellipsis,
            maxLines: 4,
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    news.date,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Hacen_Liner",
                        fontSize: 12.0,
                        fontWeight: FontWeight.w600),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ShareMenu(
                                  color: color,
                                  lang: lang,
                                  message: news.sourceLink)));
                    },
                    child: Icon(
                      Icons.share,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
    final newsCard = new Container(
      child: newsCardContent,
      height: horizontal ? 124.0 : 154.0,
      margin: horizontal
          ? (lang == 1
              ? EdgeInsets.only(right: 46)
              : new EdgeInsets.only(left: 46))
          : new EdgeInsets.only(top: 72.0),
      decoration: new BoxDecoration(
        color: new Color(color),
        shape: BoxShape.rectangle,
        borderRadius: new BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: new Offset(0.0, 10.0),
          ),
        ],
      ),
    );

    return new GestureDetector(
        onTap: horizontal
            ? () => Navigator.of(context).push(
                  new PageRouteBuilder(
                    pageBuilder: (_, __, ___) =>
                        new DetailPage(news, color, lang, size),
                    transitionsBuilder: (context, animation, secondaryAnimation,
                            child) =>
                        new FadeTransition(opacity: animation, child: child),
                  ),
                )
            : null,
        child: new Container(
          margin: const EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 24.0,
          ),
          child: new Stack(
            textDirection: TextDirection.rtl,
            children: <Widget>[
              newsCard,
              newsThumbnail,
            ],
          ),
        ));
  }
}
