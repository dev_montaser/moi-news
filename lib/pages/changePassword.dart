import 'dart:convert';

import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:moi/model/theme_model.dart';
import 'package:moi/new_chat/widgets/custombutton.dart';
import 'package:moi/new_chat/widgets/customtextinput.dart';
import 'package:moi/settings/app_localizations.dart';
import 'package:provider/provider.dart';

class ChangePassword extends StatefulWidget {
  ThemeModel model;

  ChangePassword({Key key, this.model}) : super(key: key);

  @override
  _ChatterSignUpState createState() => _ChatterSignUpState();
}

class _ChatterSignUpState extends State<ChangePassword> {
  String oldpass;
  String newpass;
  String confirmpass;

  // String username;

  bool signingup = false;

  Future<String> attemptLogIn() async {
    var res = await http.post("https://stgnms.moi.gov.ae/connect/token", body: {
      "username": 'admin',
      "password": '12345678',
      "grant_type": 'password'
    });
    var jsonslide = jsonDecode(res.body);
    if (res.statusCode == 200) {
      /* var result = await http.post(
          "https://stgnms.moi.gov.ae/connect/ChangePassword",
          headers: {
            'Authorization':'Bearer ${jsonslide['access_token']}'
          },
          body: {
            "username": 'admin',
            "password": '123456789',
            "NewPassword": '12345678'
          }
      );
      if(result.statusCode==200){
        print('done ${result.body}');
      }else{
        print('not done ${result.body}');
      }*/
      var result = await http.get(
        "https://stgnms.moi.gov.ae/api/Mobile/GetImportantReports",
        headers: {'Authorization': 'Bearer ${jsonslide['access_token']}'},
      );
      if (result.statusCode == 200) {
        print('done ${result.body}');
      } else {
        print('not done ${result.body}');
      }
    }
    ;
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: widget.model,
      child: Consumer<ThemeModel>(
        builder: (_, model, __) => ModalProgressHUD(
          inAsyncCall: signingup,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: model.primaryMainColor,
            ),
            body: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                verticalDirection: VerticalDirection.down,
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  GestureDetector(
                    onTap: () {
                      attemptLogIn();
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 75),
                      child: Image(
                        image: new AssetImage(
                          'images/logo_only.png',
                        ),
                        fit: BoxFit.contain,
                        colorBlendMode: BlendMode.clear,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  CustomTextInput(
                      hintText:
                          AppLocalizations.of(context).translate('opassword'),
                      leading: Icons.lock,
                      obscure: true,
                      userTyped: (val) {
                        oldpass = val;
                      },
                      color: model.primaryMainColor),
                  SizedBox(
                    height: 5,
                  ),
                  CustomTextInput(
                      hintText:
                          AppLocalizations.of(context).translate('npassword'),
                      leading: Icons.lock,
                      obscure: true,
                      userTyped: (val) {
                        newpass = val;
                      },
                      color: model.primaryMainColor),
                  SizedBox(
                    height: 5,
                  ),
                  CustomTextInput(
                      hintText:
                          AppLocalizations.of(context).translate('cpassword'),
                      leading: Icons.lock,
                      obscure: true,
                      userTyped: (val) {
                        confirmpass = val;
                      },
                      color: model.primaryMainColor),
                  SizedBox(
                    height: 30,
                  ),
                  Hero(
                    tag: 'signupbutton',
                    child: CustomButton(
                      onpress: () async {
                        if (newpass != null &&
                            oldpass != null &&
                            confirmpass != null) {
                          if (newpass != confirmpass) {
                            EdgeAlert.show(context,
                                title: 'Sending Failed',
                                description: 'missmatch password.',
                                gravity: EdgeAlert.BOTTOM,
                                icon: Icons.error,
                                backgroundColor: model.primaryMainColor);
                          } else {
                            setState(() {
                              signingup = true;
                            });
                          }
                        } else {
                          EdgeAlert.show(context,
                              title: 'Sending Failed',
                              description: 'all fields are required.',
                              gravity: EdgeAlert.BOTTOM,
                              icon: Icons.error,
                              backgroundColor: model.primaryMainColor);
                        }
                      },
                      text: AppLocalizations.of(context).translate('send'),
                      accentColor: Colors.white,
                      mainColor: model.primaryMainColor,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
