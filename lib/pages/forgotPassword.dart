import 'dart:convert';

import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:moi/model/theme_model.dart';
import 'package:moi/new_chat/widgets/custombutton.dart';
import 'package:moi/new_chat/widgets/customtextinput.dart';
import 'package:moi/settings/app_localizations.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class ForgotPassword extends StatefulWidget {
  ThemeModel model;

  ForgotPassword({Key key, this.model}) : super(key: key);

  @override
  _ChatterSignUpState createState() => _ChatterSignUpState();
}

class _ChatterSignUpState extends State<ForgotPassword> {
  String email;

  // String username;

  bool signingup = false;

  Future<String> forgotPassword() async {
    setState(() {
      signingup = true;
    });

    var res = await http
        .post("https://stgnms.moi.gov.ae/connect/ForgotPassword", body: {
      "email": email,
    });
    var jsonslide = jsonDecode(res.body);
    print('jsons ${jsonslide}');
    if (res.statusCode == 200) {
      setState(() {
        signingup = false;
      });
      EdgeAlert.show(context,
          title: 'done!',
          description: 'password send to your email',
          gravity: EdgeAlert.BOTTOM,
          icon: Icons.done,
          backgroundColor: widget.model.primaryMainColor);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => MyApp()),
          (Route<dynamic> route) => false);
    } else {
      setState(() {
        signingup = false;
      });
      EdgeAlert.show(context,
          title: 'Uh oh!',
          description: jsonslide['error_description'],
          gravity: EdgeAlert.BOTTOM,
          icon: Icons.error,
          backgroundColor: widget.model.primaryMainColor);
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
          builder: (_, model, __) => ModalProgressHUD(
            inAsyncCall: signingup,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: model.primaryMainColor,
              ),
              body: Container(
                height: MediaQuery.of(context).size.height,
                // margin: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.2),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    verticalDirection: VerticalDirection.down,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 75),
                        child: Image(
                          image: new AssetImage(
                            'images/logo_only.png',
                          ),
                          fit: BoxFit.contain,
                          colorBlendMode: BlendMode.clear,
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                      CustomTextInput(
                        hintText:
                            AppLocalizations.of(context).translate('email'),
                        leading: Icons.mail,
                        keyboard: TextInputType.emailAddress,
                        obscure: false,
                        userTyped: (value) {
                          email = value;
                        },
                        color: model.primaryMainColor,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Hero(
                        tag: 'signupbutton',
                        child: CustomButton(
                          onpress: () async {
                            if (email != null) {
                              forgotPassword();
                            } else {
                              EdgeAlert.show(context,
                                  title: 'email error',
                                  description: 'email field is required.',
                                  gravity: EdgeAlert.BOTTOM,
                                  icon: Icons.error,
                                  backgroundColor: model.primaryMainColor);
                            }
                          },
                          text: AppLocalizations.of(context).translate('send'),
                          accentColor: Colors.white,
                          mainColor: model.primaryMainColor,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      GestureDetector(
                          onTap: () {
                            Navigator.pushReplacementNamed(context, 'login');
                          },
                          child: Text(
                            AppLocalizations.of(context).translate('or'),
                            style: TextStyle(
                                fontFamily: 'Cairo-Regular',
                                fontSize: 14,
                                color: model.primaryMainColor),
                          )),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.1,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
