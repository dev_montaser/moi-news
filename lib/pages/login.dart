import 'dart:convert';

import 'package:edge_alert/edge_alert.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:moi/model/theme_model.dart';
import 'package:moi/new_chat/widgets/custombutton.dart';
import 'package:moi/new_chat/widgets/customtextinput.dart';
import 'package:moi/pages/forgotPassword.dart';
import 'package:moi/settings/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  ThemeModel model;

  LoginScreen({Key key, this.title, this.model}) : super(key: key);

  final String title;

  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  String email;
  String password;
  final _auth = FirebaseAuth.instance;

  //final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences prefs;

  bool isLoading = false;
  bool isLoggedIn = false;

  // User currentUser;

  @override
  void initState() {
    super.initState();
    isSignedIn();
  }

  void isSignedIn() async {
    this.setState(() {
      isLoading = true;
    });

    prefs = await SharedPreferences.getInstance();

    //  isLoggedIn = await googleSignIn.isSignedIn();
    /* if (isLoggedIn) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Home(widget.model)),
      );
    }*/

    this.setState(() {
      isLoading = false;
    });
  }

  Future<String> attemptLogIn() async {
    this.setState(() {
      isLoading = true;
    });
    var res = await http.post("https://stgnms.moi.gov.ae/connect/token", body: {
      "username": email,
      "password": password,
      "grant_type": 'password'
    });
    var jsonslide = jsonDecode(res.body);
    //print('body ${jsonslide}');
    if (res.statusCode == 200) {
      prefs.setString("username", email);
      prefs.setString("password", password);
      prefs.setString("token", jsonslide['access_token']);
      try {
        final loggedUser = await _auth.signInWithEmailAndPassword(
            email: '$email@chat.com', password: password);
        if (loggedUser != null) {
          setState(() {
            isLoading = false;
          });
          await prefs.setString("id", loggedUser.user.uid);
          print('in 1 ${loggedUser.user.uid}');
          /* Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home(widget.model)));*/
          Navigator.pushReplacementNamed(context, 'home');
        } else {
          print('in 2 ${loggedUser.user.uid}');

          final newUser = await _auth.createUserWithEmailAndPassword(
            email: '$email@chat.com',
            password: password,
          );
          if (newUser != null) {
            /*UpdateUserInfo info = UpdateUserInfo();
                              info.displayName = name;
                              await newUser.user.updateProfile(info);*/
            setState(() {
              isLoading = false;
            });

            await prefs.setString("id", newUser.user.uid);
            print('in 2 ${loggedUser.user.uid}');

            /* Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home(widget.model)));*/
            Navigator.pushReplacementNamed(context, 'home');
          } else {
            EdgeAlert.show(context,
                title: 'Login Failed',
                description: 'error in firebase',
                gravity: EdgeAlert.BOTTOM,
                icon: Icons.error,
                backgroundColor: widget.model.primaryMainColor);
          }
        }
      } catch (e) {
        try {
          final newUser = await _auth.createUserWithEmailAndPassword(
            email: '$email@chat.com',
            password: password,
          );
          if (newUser != null) {
            /*UpdateUserInfo info = UpdateUserInfo();
                              info.displayName = name;
                              await newUser.user.updateProfile(info);*/
            setState(() {
              isLoading = false;
            });

            await prefs.setString("id", newUser.user.uid);
            print('in 3 ${newUser.user.uid}');

            /* Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home(widget.model)));*/
            Navigator.pushReplacementNamed(context, 'home');
          } else {
            setState(() {
              isLoading = false;
            });
            print(e.toString());
            EdgeAlert.show(context,
                title: 'Login Failed',
                description: e.toString(),
                gravity: EdgeAlert.BOTTOM,
                icon: Icons.error,
                backgroundColor: widget.model.primaryMainColor);
          }
        } catch (e) {
          EdgeAlert.show(context,
              title: 'Login Failed',
              description: e.toString(),
              gravity: EdgeAlert.BOTTOM,
              icon: Icons.error,
              backgroundColor: widget.model.primaryMainColor);
        }
      }
    } else {
      this.setState(() {
        isLoading = false;
      });
      EdgeAlert.show(context,
          title: 'Login Failed',
          description: jsonslide['error_description'],
          gravity: EdgeAlert.BOTTOM,
          icon: Icons.error,
          backgroundColor: widget.model.primaryMainColor);
    }
    return null;
  }

  /*Future<Null> handleSignIn() async {
    prefs = await SharedPreferences.getInstance();

    this.setState(() {
      isLoading = true;
    });

    //GoogleSignInAccount googleUser = await googleSignIn.signIn();
 //   GoogleSignInAuthentication googleAuth = await googleUser.authentication;

  /*  final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );*/

    User firebaseUser =
        (await firebaseAuth.signInWithCredential(credential)).user;

    if (firebaseUser != null) {
      // Check is already sign up
      final QuerySnapshot result = await FirebaseFirestore.instance
          .collection('users')
          .where('id', isEqualTo: firebaseUser.uid)
          .get();
      final List<DocumentSnapshot> documents = result.docs;
      if (documents.length == 0) {
        // Update data to server if new user
        FirebaseFirestore.instance
            .collection('users')
            .doc(firebaseUser.uid)
            .set({
          'nickname': firebaseUser.displayName,
          'photoUrl': firebaseUser.photoURL,
          'id': firebaseUser.uid,
          'createdAt': DateTime.now().millisecondsSinceEpoch.toString(),
          'chattingWith': null
        });

        // Write data to local
        currentUser = firebaseUser;
        await prefs.setString('id', currentUser.uid);
        await prefs.setString('nickname', currentUser.displayName);
        await prefs.setString('photoUrl', currentUser.photoURL);
      } else {
        // Write data to local
        await prefs.setString('id', documents[0].data()['id']);
        await prefs.setString('nickname', documents[0].data()['nickname']);
        await prefs.setString('photoUrl', documents[0].data()['photoUrl']);
        await prefs.setString('aboutMe', documents[0].data()['aboutMe']);
      }
      EdgeAlert.show(context,
          title: 'Uh oh!',
          description: 'Sign in success.',
          gravity: EdgeAlert.BOTTOM,
          icon: Icons.done,
          backgroundColor: Colors.red[900]);
      this.setState(() {
        isLoading = false;
      });

      Navigator.push(context, MaterialPageRoute(builder: (context) => Home(widget.model)));
    } else {
      EdgeAlert.show(context,
          title: 'Uh oh!',
          description: 'Sign in fail.',
          gravity: EdgeAlert.BOTTOM,
          icon: Icons.error,
          backgroundColor: Colors.red[900]);
      this.setState(() {
        isLoading = false;
      });
    }
  }*/

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
            builder: (_, model, __) => ModalProgressHUD(
                  inAsyncCall: isLoading,
                  child: Scaffold(
                      body: SingleChildScrollView(
                    child: Container(
                      height: MediaQuery.of(context).size.height,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 75),
                              child: Image(
                                image: new AssetImage(
                                  'images/logo_only.png',
                                ),
                                fit: BoxFit.contain,
                                colorBlendMode: BlendMode.clear,
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.02,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 7.0),
                              child: Icon(
                                Icons.exit_to_app,
                                size: 50,
                                color: model.primaryMainColor,
                              ),
                            ),
                            Text(
                              AppLocalizations.of(context).translate('login'),
                              style: TextStyle(
                                  fontFamily: 'Cairo-Bold',
                                  fontSize: 18,
                                  color: model.primaryMainColor),
                            ),
                            CustomTextInput(
                                hintText: AppLocalizations.of(context)
                                    .translate('email'),
                                leading: Icons.mail,
                                obscure: false,
                                keyboard: TextInputType.emailAddress,
                                userTyped: (val) {
                                  email = val;
                                },
                                color: model.primaryMainColor),
                            SizedBox(
                              height: 0,
                            ),
                            CustomTextInput(
                                hintText: AppLocalizations.of(context)
                                    .translate('password'),
                                leading: Icons.lock,
                                obscure: true,
                                userTyped: (val) {
                                  password = val;
                                },
                                color: model.primaryMainColor),
                            SizedBox(
                              height: 30,
                            ),
                            Hero(
                              tag: 'loginbutton',
                              child: CustomButton(
                                text: AppLocalizations.of(context)
                                    .translate('login'),
                                accentColor: Colors.white,
                                mainColor: model.primaryMainColor,
                                onpress: () async {
                                  if (password != null && email != null) {
                                    attemptLogIn();
                                    /*  setState(() {
                          isLoading = true;
                        });
                        try {
                          final loggedUser =
                              await _auth.signInWithEmailAndPassword(
                                  email: email, password: password);
                          if (loggedUser != null) {
                            setState(() {
                              isLoading = false;
                            });
                            prefs.setString("id", loggedUser.user.uid);
                           /* Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home(widget.model)));*/
                            Navigator.pushReplacementNamed(context, 'home');

                          }
                        } catch (e) {
                          setState(() {
                            isLoading = false;
                          });
                          EdgeAlert.show(context,
                              title: 'Login Failed',
                              description: e.toString(),
                              gravity: EdgeAlert.BOTTOM,
                              icon: Icons.error,
                              backgroundColor: model.primaryMainColor);
                        }*/
                                  } else {
                                    EdgeAlert.show(context,
                                        title: 'Uh oh!',
                                        description:
                                            'Please enter the email and password.',
                                        gravity: EdgeAlert.BOTTOM,
                                        icon: Icons.error,
                                        backgroundColor:
                                            model.primaryMainColor);
                                  }
                                  // Navigator.pushReplacementNamed(context, '/chat');
                                },
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              ForgotPassword(
                                                model: model,
                                              )));
                                },
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate('forgot'),
                                  style: TextStyle(
                                      fontFamily: 'Cairo-Regular',
                                      fontSize: 14,
                                      color: model.primaryMainColor),
                                )),
                            FlatButton(
                              onPressed: () {
                                model.appLocal == Locale('ar')
                                    ? model.changeLanguage(Locale('en'))
                                    : model.changeLanguage(Locale('ar'));
                              },
                              child: Text(
                                model.appLocal.languageCode == 'ar'
                                    ? 'English'
                                    : 'عربى',
                                style: TextStyle(
                                    fontFamily: 'Cairo-Regular',
                                    fontSize: 14,
                                    color: model.primaryMainColor),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.1,
                            ),
                            /*   Hero(
                  tag: 'footer',
                  child: Text(
                    'Made with ♥ by ishandeveloper',
                    style: TextStyle(fontFamily: 'Poppins'),
                  ),
                ),*/
                            /*FlatButton(
                            onPressed: handleSignIn,
                            child: Text(
                              'SIGN IN WITH GOOGLE',
                              style: TextStyle(fontSize: 16.0),
                            ),
                            color: Color(0xffdd4b39),
                            highlightColor: Color(0xffff7f7f),
                            splashColor: Colors.transparent,
                            textColor: Colors.white,
                            padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0)
                        ),*/
                          ],
                        ),
                      ),
                    ),

                    // Loading
                  )),
                )));
  }
}
