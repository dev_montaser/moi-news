import 'dart:async';
import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GooMap extends StatefulWidget {
  //GooMap({Key key}) : super(key: key);

  final LatLng location;
  final int color;

  GooMap({this.location, this.color});

  @override
  _GooMapState createState() => _GooMapState();
}

class _GooMapState extends State<GooMap> {
  // Location
  LatLng _locationData;

  // Maps
  Set<Marker> _markers = HashSet<Marker>();
  Set<Polygon> _polygons = HashSet<Polygon>();
  Set<Circle> _circles = HashSet<Circle>();
  GoogleMapController _googleMapController;
  Completer<GoogleMapController> _controller = Completer();
  BitmapDescriptor _markerIcon;
  List<LatLng> polygonLatLngs = List<LatLng>();
  double radius;
  LatLng inilatlng;

  //ids
  int _polygonIdCounter = 1;
  int _circleIdCounter = 1;
  int _markerIdCounter = 1;

  // Type controllers
  bool _isPolygon = true; //Default
  bool _isMarker = true;
  bool _isCircle = false;

  void addPoints() {
    inilatlng = LatLng(
        GeoJson.EG[(GeoJson.EG.length - 1).toInt()][1].toDouble(),
        GeoJson.EG[(GeoJson.EG.length / 6).toInt()][0].toDouble());
    for (var i = 0; i < GeoJson.EG.length; i++) {
      var ltlng =
          LatLng(GeoJson.EG[i][1].toDouble(), GeoJson.EG[i][0].toDouble());
      polygonLatLngs.add(ltlng);
    }
  }

  Widget mapState() {
    return GoogleMap(
      onMapCreated: _onMapCreated,
      initialCameraPosition:
          CameraPosition(target: LatLng(78.9629, 20.5937), zoom: 0.0),
      mapToolbarEnabled: false,
      zoomControlsEnabled: true,
      scrollGesturesEnabled: true,
      zoomGesturesEnabled: true,
      myLocationButtonEnabled: false,
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer())),
      //markers: markers,
      polygons: _polygons,
    );
  }

  String _mapStyle;

  //bool reg_visible=false;
  @override
  void initState() {
    addPoints();
    List<Polygon> addPolygon = [
      Polygon(
        polygonId: PolygonId('India'),
        points: polygonLatLngs,
        //  consumeTapEvents: true,
        zIndex: 10,
        onTap: () {
          setState(() {
            // reg_visible=true;
          });
        },
        visible: true,
        geodesic: true,
        strokeColor: Colors.grey,
        strokeWidth: 1,
        fillColor: Color(widget.color),
      ),
    ];
    _polygons.addAll(addPolygon);

    rootBundle.loadString('assets/map_style.txt').then((string) {
      _mapStyle = string;
    });
    super.initState();
    // If I want to change the marker icon:
    // _setMarkerIcon();
    _locationData = widget.location;
  }

  // This function is to change the marker icon
  void _setMarkerIcon() async {
    _markerIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'images/content.png');
  }

  // Draw Polygon to the map
  void _setPolygon() {
    final String polygonIdVal = 'polygon_id_$_polygonIdCounter';
    _polygons.add(Polygon(
      polygonId: PolygonId(polygonIdVal),
      points: polygonLatLngs,
      strokeWidth: 2,
      strokeColor: Colors.red,
      fillColor: Colors.red.withOpacity(0.15),
    ));
  }

  // Set circles as points to the map
  void _setCircles(LatLng point) {
    final String circleIdVal = 'circle_id_$_circleIdCounter';
    _circleIdCounter++;
    print(
        'Circle | Latitude: ${point.latitude}  Longitude: ${point.longitude}  Radius: $radius');
    _circles.add(Circle(
        circleId: CircleId(circleIdVal),
        center: point,
        radius: radius,
        fillColor: Colors.redAccent.withOpacity(0.5),
        strokeWidth: 3,
        strokeColor: Colors.redAccent));
  }

  // Set Markers to the map
  void _setMarkers(LatLng point) {
    final String markerIdVal = 'marker_id_$_markerIdCounter';
    _markerIdCounter++;
    _setMarkerIcon();
    setState(() {
      print(
          'Marker | Latitude: ${point.latitude}  Longitude: ${point.longitude}');
      _markers.add(
        Marker(
          markerId: MarkerId(markerIdVal),
          position: point,
          consumeTapEvents: false,
          infoWindow: InfoWindow(
            title: 'number',
            snippet: 'Description here',
          ),
          icon: _markerIcon,
        ),
      );
    });
  }

  // Start the map with this marker setted up
  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    _googleMapController = controller;
    _googleMapController.setMapStyle(_mapStyle);
    setState(() {
      /*  _markers.add(
        Marker(
          markerId: MarkerId('0'),
          position: inilatlng,
          flat: true,

          alpha: 20,
          infoWindow:
          InfoWindow(title: 'number', snippet: 'Description here',),
          //icon: _markerIcon,
        ),
      );*/
    });
  }

  /* Widget _fabPolygon() {
    return FloatingActionButton.extended(
      onPressed: () {
        //Remove the last point setted at the polygon
        setState(() {
          polygonLatLngs.removeLast();
        });
      },
      icon: Icon(Icons.undo),
      label: Text('Undo point'),
      backgroundColor: Colors.orange,
    );
  }*/

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 70),
      /* floatingActionButton:
        polygonLatLngs.length > 0 && _isPolygon ? _fabPolygon() : null,*/
      child: GoogleMap(
        onMapCreated: _onMapCreated,
        tiltGesturesEnabled: true,
        initialCameraPosition: CameraPosition(
          target: inilatlng,
          zoom: 3.0,
        ),
        mapType: MapType.normal,
        mapToolbarEnabled: false,
        zoomGesturesEnabled: true,

        markers: _markers,

        onCameraMove: (position) {
          setState(() {
            //   reg_visible=false;
          });
        },
        // circles: _circles,
        gestureRecognizers: Set()
          ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer())),
        polygons: _polygons,

        //liteModeEnabled: true,
        // myLocationEnabled: true,
        onTap: (point) {
          print('${GeoJson.IN.contains(point)} $point');

          setState(() {
            _markers.clear();
            _markerIdCounter = 0;
            //  reg_visible=true;

            _setMarkers(point);
          });

          /* if (_isPolygon) {
                  setState(() {
                    polygonLatLngs.add(point);
                    _setPolygon();
                  });
                } else if (_isMarker) {
                  setState(() {
                    _markers.clear();
                    _setMarkers(point);
                  });
                } else if (_isCircle) {
                  setState(() {
                    _circles.clear();
                    _setCircles(point);
                  });
                }*/
        },
      ),
      // mapState(),
      /*  Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                children: <Widget>[
                  RaisedButton(
                      color: Colors.black54,
                      onPressed: () {
                        _isPolygon = true;
                        _isMarker = false;
                        _isCircle = false;
                      },
                      child: Text(
                        'Polygon',
                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                      )),
                  RaisedButton(
                      color: Colors.black54,
                      onPressed: () {
                        _isPolygon = false;
                        _isMarker = true;
                        _isCircle = false;
                      },
                      child: Text('Marker',
                          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white))),
                  RaisedButton(
                      color: Colors.black54,
                      onPressed: () {
                        _isPolygon = false;
                        _isMarker = false;
                        _isCircle = true;
                        radius = 50;
                        return showDialog(
                            context: context,
                            child: AlertDialog(
                              backgroundColor: Colors.grey[900],
                              title: Text(
                                'Choose the radius (m)',
                                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                              ),
                              content: Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Material(
                                    color: Colors.black,
                                    child: TextField(
                                      style: TextStyle(fontSize: 16, color: Colors.white),
                                      decoration: InputDecoration(
                                        icon: Icon(Icons.zoom_out_map),
                                        hintText: 'Ex: 100',
                                        suffixText: 'meters',
                                      ),
                                      keyboardType:
                                      TextInputType.numberWithOptions(),
                                      onChanged: (input) {
                                        setState(() {
                                          radius = double.parse(input);
                                        });
                                      },
                                    ),
                                  )),
                              actions: <Widget>[
                                FlatButton(
                                    onPressed: () => Navigator.pop(context),
                                    child: Text(
                                      'Ok',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,),
                                    )),
                              ],
                            ));
                      },
                      child: Text('Circle',
                          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)))
                ],
              ),
            )*/
    );
  }
}

class GeoJson {
  static const List EG = [
    [34.9226, 29.50133],
    [34.64174, 29.09942],
    [34.42655, 28.34399],
    [34.15451, 27.8233],
    [33.92136, 27.6487],
    [33.58811, 27.97136],
    [33.13676, 28.41765],
    [32.42323, 29.85108],
    [32.32046, 29.76043],
    [32.73482, 28.70523],
    [33.34876, 27.69989],
    [34.10455, 26.14227],
    [34.47387, 25.59856],
    [34.79507, 25.03375],
    [35.69241, 23.92671],
    [35.49372, 23.75237],
    [35.52598, 23.10244],
    [36.69069, 22.20485],
    [36.86623, 22],
    [32.9, 22],
    [29.02, 22],
    [25, 22],
    [25.0, 25.682499996361],
    [25.0, 29.23865452953346],
    [24.70007, 30.04419],
    [24.95762, 30.6616],
    [24.80287, 31.08929],
    [25.16482, 31.56915],
    [26.49533, 31.58568],
    [27.45762, 31.32126],
    [28.45048, 31.02577],
    [28.91353, 30.87005],
    [29.68342, 31.18686],
    [30.09503, 31.4734],
    [30.97693, 31.55586],
    [31.68796, 31.4296],
    [31.96041, 30.9336],
    [32.19247, 31.26034],
    [32.99392, 31.02407],
    [33.7734, 30.96746],
    [34.26544, 31.21936],
    [34.9226, 29.50133]
  ];
  static const List IN = [
    [77.83745079947457, 35.494009507787766],
    [78.91226891471322, 34.32193634697579],
    [78.81108646028574, 33.50619802503242],
    [79.20889163606857, 32.994394639613716],
    [79.17612877799553, 32.48377981213771],
    [78.45844648632601, 32.61816437431273],
    [78.73889448437401, 31.515906073527063],
    [79.7213668151071, 30.882714748654728],
    [81.11125613802932, 30.183480943313402],
    [80.4767212259174, 29.72986522065534],
    [80.08842451367627, 28.79447011974014],
    [81.05720258985203, 28.416095282499043],
    [81.99998742058497, 27.925479234319994],
    [83.30424889519955, 27.36450572357556],
    [84.6750179381738, 27.234901231387536],
    [85.25177859898338, 26.72619843190634],
    [86.02439293817918, 26.63098460540857],
    [87.22747195836628, 26.397898057556077],
    [88.06023766474982, 26.41461538340249],
    [88.17480431514092, 26.81040517832595],
    [88.04313276566123, 27.445818589786825],
    [88.12044070836987, 27.876541652939594],
    [88.73032596227856, 28.086864732367516],
    [88.81424848832054, 27.29931590423936],
    [88.83564253128938, 27.098966376243762],
    [89.74452762243884, 26.719402981059957],
    [90.37327477413407, 26.87572418874288],
    [91.21751264848643, 26.808648179628022],
    [92.03348351437509, 26.83831045176356],
    [92.10371178585973, 27.452614040633208],
    [91.69665652869668, 27.77174184825166],
    [92.50311893104364, 27.89687632904645],
    [93.41334760943268, 28.640629380807226],
    [94.56599043170294, 29.277438055939985],
    [95.40480228066464, 29.03171662039213],
    [96.11767866413103, 29.452802028922466],
    [96.58659061074749, 28.830979519154344],
    [96.24883344928779, 28.41103099213444],
    [97.32711388549004, 28.26158274994634],
    [97.40256147663612, 27.88253611908544],
    [97.0519885599681, 27.69905894623315],
    [97.1339990580153, 27.083773505149964],
    [96.41936567585097, 27.264589341739224],
    [95.12476769407496, 26.5735720891323],
    [95.1551534362626, 26.001307277932085],
    [94.60324913938538, 25.162495428970402],
    [94.55265791217164, 24.675238348890332],
    [94.10674197792505, 23.85074087167348],
    [93.3251876159428, 24.078556423432204],
    [93.28632693885928, 23.043658352139005],
    [93.06029422401463, 22.70311066333557],
    [93.16612755734836, 22.278459580977103],
    [92.67272098182556, 22.041238918541254],
    [92.14603478390681, 23.627498684172593],
    [91.86992760617132, 23.624346421802784],
    [91.70647505083211, 22.985263983649183],
    [91.15896325069971, 23.50352692310439],
    [91.46772993364367, 24.072639471934792],
    [91.91509280799443, 24.13041372323711],
    [92.37620161333481, 24.976692816664965],
    [91.79959598182207, 25.147431748957317],
    [90.8722107279121, 25.132600612889547],
    [89.92069258012185, 25.26974986419218],
    [89.83248091019962, 25.96508209889548],
    [89.35509402868729, 26.014407253518073],
    [88.56304935094977, 26.44652558034272],
    [88.2097892598025, 25.76806570078271],
    [88.93155398962308, 25.238692328384776],
    [88.30637251175602, 24.866079413344206],
    [88.08442223506242, 24.501657212821925],
    [88.69994022009092, 24.23371491138856],
    [88.52976972855377, 23.631141872649163],
    [88.87631188350309, 22.879146429937826],
    [89.03196129756623, 22.055708319582976],
    [88.88876590368542, 21.690588487224748],
    [88.20849734899521, 21.703171698487807],
    [86.97570438024027, 21.49556163175521],
    [87.03316857294887, 20.743307806882413],
    [86.49935102737378, 20.151638495356607],
    [85.0602657409097, 19.4785788029711],
    [83.94100589390001, 18.302009792549725],
    [83.18921715691785, 17.67122142177898],
    [82.19279218946592, 17.016636053937813],
    [82.19124189649719, 16.556664130107848],
    [81.69271935417748, 16.310219224507904],
    [80.79199913933014, 15.951972357644491],
    [80.32489586784388, 15.899184882058348],
    [80.02506920768644, 15.136414903214147],
    [80.2332735533904, 13.835770778859981],
    [80.28629357292186, 13.006260687710833],
    [79.8625468281285, 12.056215318240888],
    [79.85799930208682, 10.35727509199711],
    [79.340511509116, 10.30885427493962],
    [78.88534549348918, 9.546135972527722],
    [79.18971967968828, 9.216543687370148],
    [78.2779407083305, 8.933046779816934],
    [77.94116539908435, 8.252959092639742],
    [77.53989790233794, 7.965534776232333],
    [76.59297895702167, 8.89927623131419],
    [76.13006147655108, 10.299630031775521],
    [75.74646731964849, 11.308250637248307],
    [75.39610110870957, 11.781245022015824],
    [74.86481570831681, 12.741935736537897],
    [74.61671715688354, 13.99258291264968],
    [74.44385949086723, 14.617221787977696],
    [73.5341992532334, 15.99065216721496],
    [73.11990929554943, 17.928570054592498],
    [72.82090945830865, 19.208233547436166],
    [72.8244751321368, 20.419503282141534],
    [72.6305334817454, 21.356009426351008],
    [71.17527347197395, 20.757441311114235],
    [70.4704586119451, 20.877330634031384],
    [75.25864179881322, 32.2711054550405],
    [74.45155927927871, 32.7648996038055],
    [74.10429365427734, 33.44147329358685],
    [73.74994835805195, 34.31769887952785],
    [74.24020267120497, 34.74888703057125],
    [75.75706098826834, 34.50492259372132],
    [76.87172163280403, 34.65354401299274],
    [77.83745079947457, 35.494009507787766]
  ];
}
