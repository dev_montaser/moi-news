import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:moi/pages/morePages.dart';
import 'package:moi/model/theme_model.dart';
import 'package:provider/provider.dart';

import 'package:moi/settings/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class More extends StatefulWidget {
  ThemeModel model;
  BuildContext context;

  More(this.model, this.context);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<More> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
          builder: (_, model, __) => Scaffold(
            backgroundColor: Colors.white,
            appBar: new AppBar(
              backgroundColor: model.primaryMainColor,
              leading: GestureDetector(
                onTap: () {
                  //Navigator.of(context).pop();
                },
                child: Container(),
              ),
              title: new Text(
                AppLocalizations.of(widget.context).translate('more') ?? 'More',
                style: TextStyle(fontFamily: 'Cairo-Regular'),
              ),
              centerTitle: true,
              bottomOpacity: 1,
            ),
            body: Center(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    /*    GestureDetector(
                      onTap: () {
                        if(model!=null)
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => ReadLater(model: model,context: widget.context,)));
                      },
                      child: CustomBox(
                        title: AppLocalizations.of(widget.context)
                            .translate('readl'),
                        iconHome: Icons.chrome_reader_mode,
                        model: model,
                        direction:
                            model.appLocal.languageCode == 'ar' ? true : false,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Divider(
                        height: 1,
                        thickness: 2,
                      ),
                    ),*/
                    GestureDetector(
                      onTap: () {
                        if (model != null)
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Conditions(
                                        model: model,
                                        context: widget.context,
                                      )));
                      },
                      child: CustomBox(
                        title: AppLocalizations.of(widget.context)
                            .translate('conditions'),
                        iconHome: Icons.featured_play_list,
                        model: model,
                        direction:
                            model.appLocal.languageCode == 'ar' ? true : false,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Divider(
                        height: 1,
                        thickness: 2,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (model != null)
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Policy(
                                        model: model,
                                        context: widget.context,
                                      )));
                      },
                      child: CustomBox(
                        title: AppLocalizations.of(widget.context)
                            .translate('policy'),
                        iconHome: Icons.contact_mail,
                        model: model,
                        direction:
                            model.appLocal.languageCode == 'ar' ? true : false,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Divider(
                        height: 1,
                        thickness: 2,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (model != null)
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Explain(
                                        model: model,
                                        context: widget.context,
                                      )));
                      },
                      child: CustomBox(
                        title: AppLocalizations.of(widget.context)
                            .translate('explain'),
                        iconHome: Icons.screen_share,
                        model: model,
                        direction:
                            model.appLocal.languageCode == 'ar' ? true : false,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Divider(
                        height: 1,
                        thickness: 2,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        handleSignOut();
                      },
                      child: CustomBox(
                        title: AppLocalizations.of(widget.context)
                            .translate('logout'),
                        iconHome: Icons.exit_to_app,
                        model: model,
                        direction:
                            model.appLocal.languageCode == 'ar' ? true : false,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Future<Null> handleSignOut() async {
    await FirebaseAuth.instance.signOut();
    /* await googleSignIn.disconnect();
    await googleSignIn.signOut();*/

    var prefs = await SharedPreferences.getInstance();
    await prefs.setString('id', null);
    await prefs.setString('nickname', null);
    await prefs.setString('photoUrl', null);
    await prefs.setString('aboutMe', null);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => MyApp()),
        (Route<dynamic> route) => false);
  }
}

class CustomBox extends StatelessWidget {
  const CustomBox(
      {@required this.title,
      @required this.iconHome,
      @required this.model,
      this.direction});

  final title;
  final model;
  final iconHome;
  final direction;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 1.0),
      padding: EdgeInsets.all(1),
      child: Row(
        textDirection: direction ? TextDirection.rtl : TextDirection.ltr,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 1.0, left: 10, right: 10),
            child: Icon(
              iconHome,
              size: 40,
              color: model.primaryMainColor,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              title,
              textDirection: direction ? TextDirection.rtl : TextDirection.ltr,
              style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Cairo-Regular'),
            ),
          ),
        ],
      ),
    );
  }
}
