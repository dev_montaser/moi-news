import 'package:flutter/material.dart';
import 'package:moi/settings/app_localizations.dart';
import 'package:moi/utils/separator.dart';
import 'package:moi/utils/text_style.dart';

import 'package:provider/provider.dart';
import 'package:moi/model/theme_model.dart';

class ReadLater extends StatefulWidget {
  ThemeModel model;
  BuildContext context;

  ReadLater({Key key, this.model, this.context}) : super(key: key);

  @override
  _ChatterSignUpState createState() => _ChatterSignUpState();
}

class _ChatterSignUpState extends State<ReadLater> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
          builder: (_, model, __) => Scaffold(
            appBar: AppBar(
              title: Text(
                model.appLocal.languageCode == 'ar'
                    ? 'القراءة لاحقاَ'
                    : 'Read later',
                style: TextStyle(fontFamily: 'Cairo-Regular'),
              ),
              backgroundColor: model.primaryMainColor,
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              child: Container(

                  // margin: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.2),

                  ),
            ),
          ),
        ));
  }
}

class Conditions extends StatefulWidget {
  ThemeModel model;
  BuildContext context;

  Conditions({Key key, this.model, this.context}) : super(key: key);

  @override
  _ConditionState createState() => _ConditionState();
}

class _ConditionState extends State<Conditions> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
          builder: (_, model, __) => Scaffold(
            appBar: AppBar(
              title: Text(
                model.appLocal.languageCode == 'ar'
                    ? ' الشروط والاحكام'
                    : 'Terms and conditions',
                style: TextStyle(fontFamily: 'Cairo-Regular'),
              ),
              backgroundColor: model.primaryMainColor,
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(15),
                child: Column(
                  textDirection: TextDirection.rtl,
                  children: <Widget>[
                    new Text(
                      'الأحكام والشروط',
                      style: Style.headerTextStyle,
                    ),
                    new Separator(),
                    new Text(
                        AppLocalizations.of(widget.context).translate('con'),
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                            fontSize: model.article_size,
                            fontFamily: 'Cairo-Regular',
                            fontWeight: FontWeight.w400)),
                  ],
                ),
                // margin: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.2),
              ),
            ),
          ),
        ));
  }
}

class Policy extends StatefulWidget {
  ThemeModel model;
  BuildContext context;

  Policy({Key key, this.model, this.context}) : super(key: key);

  @override
  _Policy createState() => _Policy();
}

class _Policy extends State<Policy> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
          builder: (_, model, __) => Scaffold(
            appBar: AppBar(
              title: Text(
                model.appLocal.languageCode == 'ar'
                    ? 'سياسة الخصوصية'
                    : 'Privacy policy',
                style: TextStyle(fontFamily: 'Cairo-Regular'),
              ),
              backgroundColor: model.primaryMainColor,
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: <Widget>[
                    new Text(
                      'سياسة الخصوصية',
                      style: Style.headerTextStyle,
                    ),
                    new Separator(),
                    new Text(
                        AppLocalizations.of(widget.context).translate('pol'),
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                            fontSize: model.article_size,
                            fontFamily: 'Cairo-Regular',
                            fontWeight: FontWeight.w400)),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

class Explain extends StatefulWidget {
  ThemeModel model;
  BuildContext context;

  Explain({Key key, this.model, this.context}) : super(key: key);

  @override
  _Explain createState() => _Explain();
}

class _Explain extends State<Explain> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
          builder: (_, model, __) => Scaffold(
            appBar: AppBar(
              title: Text(
                model.appLocal.languageCode == 'ar'
                    ? 'شرح توضيحى'
                    : 'Explainatory Explain',
                style: TextStyle(fontFamily: 'Cairo-Regular'),
              ),
              backgroundColor: model.primaryMainColor,
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              child: Container(

                  // margin: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.2),

                  ),
            ),
          ),
        ));
  }
}
