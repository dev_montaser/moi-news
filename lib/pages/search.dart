import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'package:http/http.dart' as http;
import 'package:moi/settings/app_localizations.dart';
import 'package:moi/model/theme_model.dart';

const themeColor = Color(0xff1963b6);
const iconColor = Colors.white;
const themeGradient = Color(0x591963b);
const themeFooter = Colors.black;

class SearchPage extends StatefulWidget {
  ThemeModel model;

  SearchPage(@required this.model);

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<SearchPage> {
  var dateFormatted;
  var deviceHeight;
  var deviceWidth;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  //

  @override
  Widget build(BuildContext context) {
    dateFormatted = formatDate(DateTime.now(), [d, ' ', MM, ' ', yyyy]);
    deviceHeight = MediaQuery.of(context).size.height;
    deviceWidth = MediaQuery.of(context).size.width;
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
          builder: (context, model, __) {
            //  print("Value of primary main color is: ${model.primaryMainColor}");
            return MaterialApp(
                debugShowCheckedModeBanner: false,
                home: Scaffold(
                  appBar: AppBar(
                    backgroundColor: model.primaryMainColor,
                    title: new Text(
                      AppLocalizations.of(context).translate('searchp') ??
                          'Search page',
                      style: TextStyle(fontFamily: 'Cairo-Regular'),
                    ),
                    centerTitle: true,
                    leading: GestureDetector(
                      child: Icon(Icons.keyboard_backspace),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  body: Directionality(
                    textDirection: model.appLocal.languageCode == "ar"
                        ? TextDirection.rtl
                        : TextDirection.ltr,
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              color: Color(0xFFe8e8e8),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            padding: EdgeInsets.only(left: 10, right: 10),
                            width: MediaQuery.of(context).size.width * 0.80,
                            child: TextField(
                              style: TextStyle(fontSize: 13),
                              onSubmitted: (value) {},
                              decoration: InputDecoration(
                                icon: Icon(
                                  Icons.search,
                                ),
                                border: InputBorder.none,
                                hintText: AppLocalizations.of(context)
                                    .translate('search'),
                                hintStyle: TextStyle(
                                  fontFamily: 'Cairo-Regular',
                                ),
                                // filled: true,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if (model != null)
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            FilterPage(model)));
                            },
                            child: Icon(
                              Icons.filter_list,
                              size: 30,
                              color: Colors.black54,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ));
          },
        ));
  }
}

class FilterPage extends StatefulWidget {
  ThemeModel model;

  FilterPage(@required this.model);

  @override
  _FilterState createState() => _FilterState();
}

class _FilterState extends State<FilterPage> {
  var dateFormatted;
  var deviceHeight;
  bool daily = false,
      important = false,
      economic = false,
      breaking = false,
      reading = false,
      affair = false;
  var deviceWidth;
  int _valProvince;
  bool isLoading = false;
  List<dynamic> _dataProvince = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProvince();
  }

  void getProvince() async {
    setState(() {
      isLoading = true;
    });
    var res = await http.post("https://stgnms.moi.gov.ae/connect/token", body: {
      "username": 'admin',
      "password": '12345678',
      "grant_type": 'password'
    });
    var jsonslide = jsonDecode(res.body);
    if (res.statusCode == 200) {
      var result = await http.get(
        "https://stgnms.moi.gov.ae/api/Location/getlookup?languageId=${widget.model.appLocal.languageCode == 'ar' ? 1 : 2}",
        headers: {'Authorization': 'Bearer ${jsonslide['access_token']}'},
      );
      var countries = jsonDecode(result.body);

      if (result.statusCode == 200) {
        //  var countries=JsonDecoder(result.body);
        setState(() {
          _dataProvince = countries;
          isLoading = false; // dan kita set kedalam variable _dataProvince
        });
      } else {
        setState(() {
          isLoading = false;
        });
        print('not done ${result.body}');
      }
    }
    ;
    return null;
  }

  void search() async {
    setState(() {
      //  isLoading=true;
    });
    var res = await http.post("https://stgnms.moi.gov.ae/connect/token", body: {
      "username": 'admin',
      "password": '12345678',
      "grant_type": 'password'
    });
    var jsonslide = jsonDecode(res.body);
    if (res.statusCode == 200) {
      var result = await http.get(
        "https://stgnms.moi.gov.ae/api/SearchMobileNewsLetter?locations=67&status=1&text=عمر",
        headers: {'Authorization': 'Bearer ${jsonslide['access_token']}'},
      );
      //    var countries=jsonDecode(result.body);
      print('${result.body}');
      if (result.statusCode == 200) {
        //  var countries=JsonDecoder(result.body);
        setState(() {
          // _dataProvince =countries ;
          //  isLoading=false;// dan kita set kedalam variable _dataProvince
        });
      } else {
        setState(() {
          //  isLoading=false;
        });
        print('not done ${result.body}');
      }
    }
    ;
    return null;
  }

  @override
  Widget build(BuildContext context) {
    dateFormatted = formatDate(DateTime.now(), [d, ' ', MM, ' ', yyyy]);
    deviceHeight = MediaQuery.of(context).size.height;
    deviceWidth = MediaQuery.of(context).size.width;
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
          builder: (context, model, __) {
            //  print("Value of primary main color is: ${model.primaryMainColor}");
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              home: Scaffold(
                appBar: AppBar(
                  backgroundColor: model.primaryMainColor,
                  title: new Text(
                    AppLocalizations.of(context).translate('filter') ??
                        'Filter page',
                    style: TextStyle(fontFamily: 'Cairo-Regular'),
                  ),
                  centerTitle: true,
                  leading: GestureDetector(
                    child: Icon(Icons.keyboard_backspace),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                body: Directionality(
                  textDirection: model.appLocal.languageCode == "ar"
                      ? TextDirection.rtl
                      : TextDirection.ltr,
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          AppLocalizations.of(context).translate('selectn'),
                          style: TextStyle(
                            fontFamily: 'Cairo-Bold',
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: Colors.teal,
                                )),
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.95,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Checkbox(
                                          value: daily,
                                          onChanged: (value) {
                                            setState(() {
                                              daily = value;
                                            });
                                          },
                                        ),
                                        Text(
                                          AppLocalizations.of(context)
                                              .translate(
                                            'daily',
                                          ),
                                          style: TextStyle(
                                              fontFamily: 'Cairo-Regular'),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          value: reading,
                                          onChanged: (value) {
                                            setState(() {
                                              reading = value;
                                            });
                                          },
                                        ),
                                        Text(
                                          AppLocalizations.of(context)
                                              .translate(
                                            'read',
                                          ),
                                          style: TextStyle(
                                              fontFamily: 'Cairo-Regular'),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          value: important,
                                          onChanged: (value) {
                                            setState(() {
                                              important = value;
                                            });
                                          },
                                        ),
                                        Text(
                                          AppLocalizations.of(context)
                                              .translate(
                                            'important',
                                          ),
                                          style: TextStyle(
                                              fontFamily: 'Cairo-Regular'),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Checkbox(
                                          value: affair,
                                          onChanged: (value) {
                                            setState(() {
                                              affair = value;
                                            });
                                          },
                                        ),
                                        Text(
                                          AppLocalizations.of(context)
                                              .translate(
                                            'affair',
                                          ),
                                          style: TextStyle(
                                              fontFamily: 'Cairo-Regular'),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          value: economic,
                                          onChanged: (value) {
                                            setState(() {
                                              economic = value;
                                            });
                                          },
                                        ),
                                        Text(
                                          AppLocalizations.of(context)
                                              .translate(
                                            'economy',
                                          ),
                                          style: TextStyle(
                                              fontFamily: 'Cairo-Regular'),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          value: breaking,
                                          onChanged: (value) {
                                            setState(() {
                                              breaking = value;
                                            });
                                          },
                                        ),
                                        Text(
                                          AppLocalizations.of(context)
                                              .translate(
                                            'breaking',
                                          ),
                                          style: TextStyle(
                                              fontFamily: 'Cairo-Regular'),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          AppLocalizations.of(context).translate('country'),
                          style: TextStyle(
                            fontFamily: 'Cairo-Bold',
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        isLoading
                            ? CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(themeColor),
                              )
                            : Container(
                                width: deviceWidth * .8,
                                child: DropdownButton(
                                  isExpanded: true,
                                  elevation: 10,
                                  style: TextStyle(fontFamily: 'Cairo-Regular'),
                                  hint: Text(
                                      AppLocalizations.of(context)
                                          .translate('select'),
                                      style: TextStyle(
                                          fontFamily: 'Cairo-Regular')),
                                  value: _valProvince,
                                  items: _dataProvince.map((item) {
                                    return DropdownMenuItem(
                                      child: Text(item['name'],
                                          style: TextStyle(
                                              fontFamily: 'Cairo-Regular',
                                              color: Colors.black)),
                                      value: item['id'],
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    print('$value');
                                    setState(() {
                                      _valProvince = value;
                                    });
                                  },
                                ),
                              ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          AppLocalizations.of(context).translate('range'),
                          style: TextStyle(
                            fontFamily: 'Cairo-Bold',
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RaisedButton(
                              onPressed: () {},
                              child: Text(
                                AppLocalizations.of(context)
                                        .translate('bhour') ??
                                    '',
                                style: TextStyle(fontFamily: 'Cairo-SemiBold'),
                              ),
                            ),
                            RaisedButton(
                              onPressed: () {},
                              child: Text(
                                AppLocalizations.of(context)
                                        .translate('bweek') ??
                                    '',
                                style: TextStyle(fontFamily: 'Cairo-SemiBold'),
                              ),
                            ),
                            RaisedButton(
                              onPressed: () {},
                              child: Text(
                                AppLocalizations.of(context)
                                        .translate('bmonth') ??
                                    '',
                                style: TextStyle(fontFamily: 'Cairo-SemiBold'),
                              ),
                            ),
                            RaisedButton(
                              onPressed: () {},
                              child: Text(
                                AppLocalizations.of(context)
                                        .translate('bwhile') ??
                                    '',
                                style: TextStyle(fontFamily: 'Cairo-SemiBold'),
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          child: Align(
                            alignment: FractionalOffset.bottomCenter,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: deviceWidth * .30,
                                  child: RaisedButton(
                                    onPressed: () {},
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .translate('reset'),
                                      style: TextStyle(
                                        fontFamily: 'Cairo-Bold',
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: deviceWidth * .58,
                                  child: RaisedButton(
                                    onPressed: () {
                                      search();
                                    },
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .translate('apply'),
                                      style: TextStyle(
                                        fontFamily: 'Cairo-Bold',
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ));
  }
}
