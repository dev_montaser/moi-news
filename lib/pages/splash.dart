import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:moi/settings/app_localizations.dart';
import 'package:moi/new_chat/widgets/custombutton.dart';
import 'package:provider/provider.dart';
import 'package:moi/model/theme_model.dart';
import 'package:http/http.dart' as http;

class ChatterHome extends StatefulWidget {
  ChatterHome({Key key, this.title, this.model}) : super(key: key);
  final String title;
  ThemeModel model;

  @override
  _ChatterHomeState createState() => _ChatterHomeState();
}

class _ChatterHomeState extends State<ChatterHome>
    with TickerProviderStateMixin {
  AnimationController mainController;
  Animation mainAnimation;

  @override
  void initState() {
    super.initState();

    mainController = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );
    mainAnimation =
        ColorTween(begin: widget.model.primaryMainColor, end: Colors.grey[100])
            .animate(mainController);
    mainController.forward();
    mainController.addListener(() {
      setState(() {});
    });
  }

  /* Future<String> attemptLogIn() async {
    var res = await http.post(
        "https://stgnms.moi.gov.ae/connect/token",
        body: {
          "username": 'admin',
          "password": '12345678',
          "grant_type": 'password'
        }
    );
    print('data is ${res.statusCode==200}');
    if(res.statusCode == 200) return res.body;
    return null;
  }*/

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: widget.model,
        child: Consumer<ThemeModel>(
            builder: (_, model, __) => Scaffold(
                  backgroundColor: mainAnimation.value,
                  body: SafeArea(
                    child: Container(
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Hero(
                              tag: 'heroicon',
                              child: Icon(
                                Icons.textsms,
                                size: mainController.value * 100,
                                color: model.primaryMainColor,
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.02,
                            ),
                            Hero(
                              tag: 'HeroTitle',
                              child: Text(
                                AppLocalizations.of(context).translate('title'),
                                style: TextStyle(
                                    color: model.primaryMainColor,
                                    fontFamily: 'Poppins',
                                    fontSize: 26,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            TyperAnimatedTextKit(
                              isRepeatingAnimation: false,
                              speed: Duration(milliseconds: 60),
                              text: [
                                AppLocalizations.of(context).translate('hint')
                              ],
                              textStyle: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 12,
                                  color: model.primaryMainColor),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.15,
                            ),
                            Hero(
                              tag: 'loginbutton',
                              child: CustomButton(
                                text: AppLocalizations.of(context)
                                    .translate('login'),
                                accentColor: model.primaryMainColor,
                                onpress: () {
                                  Navigator.pushReplacementNamed(
                                      context, 'login');
                                  //  attemptLogIn();
                                },
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Hero(
                              tag: 'signupbutton',
                              child: CustomButton(
                                text: AppLocalizations.of(context)
                                    .translate('signup'),
                                accentColor: Colors.white,
                                mainColor: model.primaryMainColor,
                                onpress: () {
                                  Navigator.pushReplacementNamed(
                                      context, 'signup');
                                },
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.1,
                            ),
                            Text('Made with ♥ by ishandeveloper')
                          ],
                        ),
                      ),
                    ),
                  ),
                )));
  }
}
